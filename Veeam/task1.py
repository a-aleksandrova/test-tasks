import subprocess


def bytesTo(size, unit):
    units = {'M': 2, 'G': 3}
    power = units[unit]
    conv_size = int(size) / (1024 ** power)
    return "{0:.1f}{1}".format(conv_size, unit)  # if size is less than 100M returns 0.0G


def device_info(path):
    params = ['PATH', 'TYPE', 'SIZE', 'FSAVAIL', 'FSTYPE', 'MOUNTPOINT']
    command = ["lsblk", "-ba", "-o", ','.join(params), path]
    output = subprocess.check_output(command).decode("utf-8")
    values = output.splitlines()[1].split()

    info = dict(zip(params, values))
    info['SIZE'] = bytesTo(info['SIZE'], 'G')
    if 'FSAVAIL' in info:
        info['FSAVAIL'] = bytesTo(info['FSAVAIL'], 'M')

    return info


if __name__ == "__main__":
    file = input('Enter filepath: ')

    with open(file, 'rb') as f:
        path = f.read()
        device_params = device_info(path).values()
        print(' '.join(device_params))
