import subprocess
import sys


def info(name, option, params):
    command = ['systemctl', 'show', '{0}.{1}'.format(name, option), '-p', params]
    output = subprocess.check_output(command).decode('utf-8')
    result = {}
    for line in output.splitlines():
        param, value = line.split('=')
        result[param] = value if value else None

    if option == 'timer':
        print('{0} {1} last started {2}'.format(name,
                                                result['ActiveState'],
                                                result['LastTriggerUSec']))
    elif option == 'service':
        print('{0} {1} user {2} group {3} last started {4}'.format(name,
                                                                   result['ActiveState'],
                                                                   result['User'],
                                                                   result['Group'],
                                                                   result['ExecMainStartTimestamp']))


if __name__ == "__main__":
    option, name = sys.argv[1], sys.argv[2]
    if option == '--service':
        params = 'ActiveState,User,Group,ExecMainStartTimestamp'
    elif option == '--timer':
        params = 'ActiveState,LastTriggerUSec'
    else:
        print('Undefined option')
        print('Use:\n--service service_name\n--timer timer_name')
        sys.exit()

    info(name, option[2:], params)
