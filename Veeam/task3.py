import hashlib


def verify(file, algorithm, true_hash):
    try:
        with open(file) as f:
            content = f.read()
            hasher = hashlib.new(algorithm)
            hasher.update(content.encode())
            hash = hasher.hexdigest()

            return 'OK' if hash == true_hash else 'FAIL'

    except FileNotFoundError:
        return 'NOT FOUND'


if __name__ == "__main__":
    file = input('Enter filepath: ')

    with open(file, 'rb') as f:
        for line in f:
            file, algorithm, hash = line.decode("utf-8").split()
            print(file, verify(file, algorithm, hash))
